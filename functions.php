<?php
/**
 * Euler functions and definitions
 * @package WordPress
 */
if (!class_exists('Euler')) {

    class Euler
    {

        protected $name;
        protected $textdomain;
        var $is_ajax = false;
        var $admin_bar = false;
        var $excerpt_more = null;
        var $excerpt_length = null;
        var $content_width = null;
        protected $jquery = array();
        protected $elements = array(
            'menu' => array(),
            'meta' => array(),
            'script' => array(),
            'style' => array(),
            'sidebar' => array(),
            'image_size' => array(),
            'image_size_choose' => array(),
            'post_meta' => array('all' => array()),
            'editor_class' => array()
        );

        public function get_uri($uri = false)
        {
            return get_stylesheet_directory_uri() . ($uri === false ? '' : '/' . trim($uri, '/'));
        }

        public function __construct()
        {
            $this->name = get_stylesheet();

            $this->settings = array(
                'sections' => array(
                    'theme' => array(
                        'title' => __('Theme Options', $this->textdomain),
                        'priority' => 35,
                        'settings' => array(
                            'logo' => array(
                                'label' => __('Logo', $this->textdomain),
                                'type' => 'image',
                            ),
                            'copyright' => array(
                                'label' => __('Copyright', $this->textdomain),
                                'type' => 'text'
                            )
                        )
                    )
                )
            );

            $defaults = array(
                'textdomain' => get_stylesheet()
            );

            foreach ($defaults as $key => $value) {
                if (is_null($this->$key)) {
                    $this->$key = $value;
                }
            }

            $this->init();

            $this->setup_elements();
        }

        public function init()
        {
            
        }

        public function get_textdomain()
        {
            return $this->textdomain;
        }

        public function setup_elements()
        {

            if (is_int($this->content_width)) {
                global $content_width;
                $content_width = $this->content_width;
            }

            /* Register of Menus */
            register_nav_menus($this->get_elements('menu'));

            /* Register Sidebars */
            foreach ($this->get_elements('sidebar') as $sidebar) {

                register_sidebar(array_merge(array(
                    'before_widget' => '<div id="%1$s" class="widget %2$s">',
                    'after_widget' => '</div>',
                    'before_title' => '<h2 class="widget-title">',
                    'after_title' => '</h2>'
                                ), (array) $sidebar)
                );
            }

            /*
             * Add editor classes P.e.
             * 'title' => 'Button',
             * 'selector' => 'a',
             * 'classes' => 'button'
             */
            if ($this->get_elements('editor_class')) {
                add_filter('tiny_mce_before_init', array($this, 'tiny_mce_before_init'));
                add_filter('mce_buttons_2', array($this, 'mce_buttons_2'));
            }

            if (!is_null($this->excerpt_more)) {
                add_filter('excerpt_more', array($this, 'excerpt_more'));
            }

            if (!is_null($this->excerpt_length)) {
                add_filter('excerpt_length', array($this, 'excerpt_length'));
            }

            if (!$this->admin_bar) {
                add_filter('show_admin_bar', '__return_false');
            }

            foreach ($this->get_elements('image_size') as $key => $image_size) {
                $name = is_numeric($key) ? trim("{$image_size['width']}x{$image_size['height']}", 'x') : $key;

                $image_size = array_merge(array(
                    'width' => null,
                    'height' => null,
                    'crop' => false,
                    'label' => '',
                    'choose' => false
                        ), $image_size);

                if ($image_size['choose']) {
                    $this->add_element('image_size_choose', array($name, empty($image_size['label']) ? $name : $image_size['label']));
                }

                add_image_size($name, $image_size['width'], $image_size['height'], $image_size['crop']);
            }

            if ($this->get_elements('post_meta')) {
                add_action('add_meta_boxes', array(&$this, 'add_custom_boxes'));
            }

            remove_action('wp_head', 'wp_generator');

            /* if (!is_admin()) {
              wp_register_script('jquery-history', WP_PLUGIN_URL . '/ajax-theme/js/jquery.history.js', array('jquery'));
              wp_enqueue_script('jquery-history');
              add_action('wp_head', array($this, 'ajax_scripts'));
              } */

            add_action('after_setup_theme', array(&$this, 'after_setup_theme'));
            add_action('customize_register', array(&$this, 'customize_register'));
            add_action('wp_enqueue_scripts', array(&$this, 'wp_enqueue_scripts_styles'), 0);
            add_action('wp_head', array(&$this, 'wp_head'), 0);
            add_action('wp_footer', array(&$this, 'wp_footer'), 0);
            add_action('save_post', array(&$this, 'save_post'), 90);
            add_filter('wp_nav_menu_objects', array(&$this, 'wp_nav_menu_objects'));

            add_filter('widget_title', array(&$this, 'widget_no_title'), 100);
            add_filter('image_size_names_choose', array(&$this, 'image_size_names_choose'));


            /*
             * 
             * 
              05



             * function blog_favicon() {
              3
              echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_bloginfo('wpurl').'http://cdn3.wpbeginner.com/favicon.ico" />';
              4
              }
              5
              add_action('wp_head', 'blog_favicon');
             * 
             * 
             * 1
              add_filter( 'avatar_defaults', 'newgravatar' );
              2

              3
              function newgravatar ($avatar_defaults) {
              4
              $myavatar = get_bloginfo('template_directory') . '/images/gravatar.gif';
              5
              $avatar_defaults[$myavatar] = "WPBeginner";
              6
              return $avatar_defaults;
              7
              }


              function my_new_contactmethods( $contactmethods ) {
              // Add Twitter
              $contactmethods['twitter'] = 'Twitter';
              //add Facebook
              $contactmethods['facebook'] = 'Facebook';

              return $contactmethods;
              }
              add_filter('user_contactmethods','my_new_contactmethods',10,1);
             * 
             * <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('MiddleSidebar') ) : ?>
              2
              <!–Default sidebar info goes here–>
              3

              4
              <?php endif; ?>
             * 
             * 
              // category id in body and post class
              function category_id_class($classes) {
              global $post;
              foreach((get_the_category($post->ID)) as $category)
              $classes [] = 'cat-' . $category->cat_ID . '-id';
              return $classes;
              }
              add_filter('post_class', 'category_id_class');
              add_filter('body_class', 'category_id_class');

             * 
             */
        }

        public function add_element($type = null)
        {
            $args = func_get_args();

            if (isset($type)) {
                if (isset($args[2])) {
                    $this->elements[$type][$args[1]] = $args[2];
                } else {
                    $this->elements[$type][] = $args[1];
                }
            }

            return $this;
        }

        public function __call($methodName, $args)
        {
            if (substr($methodName, 0, 3) == 'add') {
                array_unshift($args, substr($methodName, 4));
                return call_user_method_array('add_element', $this, $args);
            }

            throw new ErrorException('Method ' . $methodName . ' not exists');
        }

        public function add_elements($type, $elements)
        {
            foreach ($elements as $key => $element) {
                if (is_int($key)) {
                    $this->add_element($type, $element);
                } else {
                    $this->add_element($type, $key, $element);
                }
            }

            return $this;
        }

        public function add_template_post_meta($key, $post_types = 'post', $args = array())
        {
            foreach ((array) $post_types as $post_type) {
                $this->elements['post_meta'][$post_type][$key] = $args;
            }

            return $this;
        }

        public function get_template_post_meta($name, $post_id = null)
        {
            if (is_null($post_id))
                $post_id = get_the_ID();

            return get_post_meta($post_id, $this->get_meta_name($name), true);
        }

        public function get_elements($type)
        {
            return (array) $this->elements[$type];
        }

        public function after_setup_theme()
        {
            load_theme_textdomain('euler', get_template_directory() . '/languages');
            load_child_theme_textdomain($this->textdomain, get_stylesheet_directory() . '/languages');

            add_editor_style();

            add_post_type_support('page', 'excerpt');

            add_theme_support('automatic-feed-links');
            add_theme_support('post-formats', array('aside', 'image', 'link', 'quote', 'status'));
            add_theme_support('post-thumbnails');
        }

        /* THEME POST META */

        function add_custom_boxes()
        {
            $post_meta = $this->get_elements('post_meta');

            $post_types = get_post_types(array('public' => true), 'names');

            foreach ($post_types as $post_type) {
                $post_meta[$post_type] = array_merge($post_meta['all'], (array) $post_meta[$post_type]);

                if (array_key_exists($post_type, $post_meta) && !empty($post_meta[$post_type])) {
                    add_meta_box($this->name . '-theme-post_meta', __('Theme custom fields', $this->textdomain), array(&$this, 'add_custom_box'), $post_type);
                }

                $this->elements['post_meta'][$post_type] = $post_meta[$post_type];
            }
        }

        public function replace_style($handle, $src, $deps = array(), $ver = false, $media = 'all')
        {
            wp_deregister_style($handle);
            wp_register_style($handle, $src, $deps, $ver, $media);
            return $this;
        }

        function add_custom_box($post)
        {
            $post_metas = $this->get_elements('post_meta');

            wp_nonce_field(plugin_basename(__FILE__), $this->name . '-theme-post_meta');

            echo '<table>';

            foreach ($post_metas[$post->post_type] as $key => $field) {
                $name = $this->get_meta_name($key);

                $value = get_post_meta($post->ID, $name, true);

                $output = "<tr><td style='width:120px; vertical-align:top;'>%s</td><td>%s %s</td></tr>";

                $label = "<label for='{$name}'>{$field['label']}</label>";
                $field_html = "";
                $description = isset($field['description']) ? '<p>' . $field['description'] . '</p>' : '';

                switch ($field['type']) {
                    case 'select':
                        $field_html .= "<select id='{$name}' name='" . $name . (($field['multiple'] == true) ? '[]' : '') . "'>";

                        foreach ($field['values'] as $key => $label) {
                            $field_html .= "<option " . ($value == $key ? 'selected' : '') . " value='{$key}'>{$label}</option>";
                        }

                        $field_html .= "</select>";

                        break;
                    case 'textarea':
                        $field_html .= "<textarea class='regular-text' id='{$name}' name='{$name}'>{$value}</textarea>";

                        break;
                    case 'date':
                        $field_html .= "<input class='medium-text' type='text' id='{$name}' value='{$value}' name='{$name}' />";

                        break;
                    case 'checkbox':
                        $field_html .= "<label><input " . ($value == 'yes' ? 'checked' : '') . " type='checkbox' id='{$name}' name='{$name}' />" . (empty($description) ? '' : '&nbsp;&nbsp;' . $field['description']) . "</label>";
                        $description = "";
                        break;
                    case 'wysiwyg':
                        ob_start();
                        wp_editor($value, $name, array('textarea_rows' => 5));
                        $field_html = ob_get_contents();
                        ob_end_clean();
                        break;
                    default:
                        $field_html = "<input class='regular-text' id='{$name}' type='text' value='{$value}' name='{$name}' />";

                        break;
                }

                printf($output, $label, $field_html, $description);
            }

            echo '</table>';
        }

        function save_post($post_id)
        {
            if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
                return;

            if (!wp_verify_nonce($_POST[$this->name . '-theme-post_meta'], plugin_basename(__FILE__)))
                return;

            /* if (!current_user_can('edit_' . $_POST['post_type'], $post_id))
              return; */

            foreach ($this->elements['post_meta'][$_POST['post_type']] as $name => $field) {
                $name = $this->get_meta_name($name);


                switch ($field['type']) {
                    case 'checkbox':
                        $value = isset($_POST[$name]) ? 'yes' : 'no';
                        break;
                    default:
                        $value = $_POST[$name];
                        break;
                }

                update_post_meta($post_id, $name, $value);
            }
        }

        function get_meta_name($name)
        {
            return 'theme_' . $this->name . '_' . $name;
        }

        public function get_theme_post_meta($post_id, $key)
        {
            if (is_null($post_id)) {
                global $post;
                $post_id = $post->ID;
            }

            return get_post_meta($post_id, $this->get_meta_name($key), true);
        }

        /* STYLES AND SCRIPTS */

        public function wp_enqueue_scripts_styles()
        {
            wp_enqueue_style('reset', get_template_directory_uri() . '/css/reset.css', array(), '1.6.1');
            wp_enqueue_style('wp', get_template_directory_uri() . '/css/wp.css');
            wp_enqueue_style(get_stylesheet(), get_stylesheet_directory_uri() . '/style.css', null, null, 'screen');

            if (is_singular() && comments_open() && get_option('thread_comments'))
                wp_enqueue_script('comment-reply');

            foreach ((array) $this->get_elements('style') as $key => $style) {
                if (is_array($style)) {
                    wp_enqueue_style($key, $style['src'], (array) $style['deps'], $style['ver'] || '', $style['media'] || 'all');
                } else {
                    wp_enqueue_style($key, $style);
                }
            }

            foreach ((array) $this->get_elements('script') as $key => $script) {
                if (is_numeric($key)) {
                    wp_enqueue_script($script);
                } else {
                    if (is_array($script)) {
                        wp_enqueue_script($key, $script['src'], (array) $script['deps'], $script['ver'] || '', true);
                    } else {
                        wp_enqueue_script($key, $script, null, null, true);
                    }
                }
            }

            if (is_admin()) {
                wp_enqueue_script('jquery-ui-datepicker');
                wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
            }
        }

        /* FILTERS */

        public function excerpt_more()
        {
            return $this->excerpt_more;
        }

        public function excerpt_length()
        {
            return $this->excerpt_length;
        }

        public function customize_register($wp_customize)
        {
            foreach ($this->settings['sections'] as $section_name => $section) {

                $section = apply_filters('theme_options', $section);

                $wp_customize->add_section($section_name, array(
                    'title' => $section['title'],
                    'priority' => $section['priority'],
                ));

                foreach ($section['settings'] as $setting_name => $setting) {

                    $wp_customize->add_setting($section_name . '_options[' . $setting_name . ']', array(
                        'default' => $setting['default'],
                        'transport' => 'refresh',
                        'capability' => 'edit_theme_options',
                    ));

                    $setting_args = array(
                        'label' => $setting['label'],
                        'section' => $section_name,
                        'settings' => $section_name . '_options[' . $setting_name . ']',
                        'type' => $setting['type']
                    );

                    switch ($setting['type']) {
                        case 'color':
                            $class = 'WP_Customize_Color_Control';
                            break;
                        case 'upload':
                            $class = 'WP_Customize_Upload_Control';
                            break;
                        case 'image':
                            $class = 'WP_Customize_Image_Control';
                            break;
                        default:
                            $class = 'WP_Customize_Control';

                            if (isset($setting['choices'])) {
                                $setting_args['choises'] = $setting['choices'];
                            }

                            break;
                    }

                    $wp_customize->add_control(new $class($wp_customize, $setting_name, $setting_args));
                }
            }


            /* $wp_customize->add_setting('logo', array(
              'default' => 'none',
              'transport' => 'refresh',
              ));

              $wp_customize->add_setting('header_textcolor', array(
              'default' => '#000000',
              'transport' => 'refresh',
              )); */
        }

        function wp_nav_menu_objects($menu)
        {
            $level = 0;
            $stack = array('0');
            foreach ($menu as $key => $item) {
                while ($item->menu_item_parent != array_pop($stack)) {
                    $level--;
                }
                $level++;
                $stack[] = $item->menu_item_parent;
                $stack[] = $item->ID;
                $menu[$key]->classes[] = 'level-' . ($level - 1);
            }
            return $menu;
        }

        public function get_theme_option($name = '')
        {
            $theme_options = get_theme_mod('theme_options', array());

            switch ($name) {
                case 'logo':
                    $output = "<a id='logo' href='" . home_url('/') . "'><img alt='" . get_bloginfo('name') . "' role='logo' src='" . $theme_options['logo'] . "' /></a>";
                    break;
                case 'logo_src':
                    $output = $theme_options['logo'];
                    break;
                case 'copyright':
                    $output = $theme_options['copyright'];
                    break;
                default:
                    $output = $theme_options[$name];
                    break;
            }

            echo apply_filters(get_stylesheet() . '_theme_option', $output);
        }

        public function wp_head()
        {
            
        }

        public function wp_footer()
        {
            
        }

        public function is_ajax()
        {
            return (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || (!strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')) ? false : true;
        }

        public function is_robots()
        {
            if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != '') {
                return true;
            } else {
                $crawlers = array('aspseek', 'abachobot', 'accoona', 'acoirobot', 'adsbot', 'alexa', 'alta vista', 'altavista', 'ask jeeves', 'baidu', 'crawler', 'croccrawler', 'dumbot', 'estyle', 'exabot', 'facebook', 'fast-enterprise', 'fast-webcrawler', 'francis', 'geonabot', 'gigabot', 'google', 'heise', 'heritrix', 'ibm', 'iccrawler', 'idbot', 'ichiro', 'lycos', 'msn', 'msrbot', 'majestic-12', 'metager', 'ng-search', 'nutch', 'omniexplorer', 'psbot', 'rambler', 'seosearch', 'scooter', 'scrubby', 'seekport', 'sensis', 'seoma', 'snappy', 'steeler', 'synoo', 'telekom', 'turnitinbot', 'tagxedo', 'voyager', 'wisenut', 'yacy', 'yahoo');
                foreach ($crawlers as $c) {
                    if (stristr($_SERVER['HTTP_USER_AGENT'], $c)) {
                        return true;
                    }
                }
                return false;
            }
        }

        public function ajax_scripts()
        {
            ?>        
            <script>
                function ajax_anchor() {
                    return window.location.hash.substring(2);
                }

                function ajax_has_anchor() {
                    return (window.location.hash == '' || window.location.hash == '!' || window.location.hash == '!/') ? false : true;
                }

                function ajax_init(f) {
                    jQuery(document).ready(function() {
                        jQuery.history.init(function(hash) {

                            f(hash.substr(1));
                        });
                    });
                }
            </script>

            <?php
        }

        public function is_post()
        {
            return ($_SERVER['REQUEST_METHOD'] == 'POST') ? true : false;
        }

        public function ajax_redirection()
        {
            if ($this->is_robots() === false && $this->is_ajax() === false) {
                global $q_config;
                if (isset($_SERVER['REDIRECT_URL']) || ($_SERVER['QUERY_STRING'] != '')) {
                    $redirect_url = isset($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : '';
                    if ($redirect_url != '' && $redirect_url[strlen($redirect_url) - 1] != '/') {
                        header('location: ' . $redirect_url . '/');
                        die();
                    } else {
                        $complete_url = get_option('siteurl');
                        if (isset($q_config)) {
                            $lang = $q_config['language'];
                            if (strpos($redirect_url, '/' . $lang . '/') !== false) {
                                $complete_url .= '/' . $lang;
                            }
                        }
                        $request_uri = substr($_SERVER['REQUEST_URI'], strlen(str_replace('index.php', '', $_SERVER['PHP_SELF'])));
                        if ($request_uri != '') {
                            header('location: ' . $complete_url . '/#!/' . $request_uri);
                            die();
                        }
                    }
                }
            }
        }

        /* TEMPLATE TAGS */

        public function meta()
        {
            foreach ((array) $this->get_elements('meta') as $name => $content) {
                echo "<meta name='{$name}' content='{$content}' />" . PHP_EOL;
            }
        }

        public function head()
        {
            get_template_part('head');
        }

        public function foot()
        {
            get_template_part('foot');
        }

        public function get_header($name = null, $callback = null)
        {
            if ($this->ajax) {
                if (!$this->is_ajax()) {
                    $this->ajax_redirection();
                    get_header($name);
                }
                if (isset($callback)) {
                    call_user_func($callback, $name);
                }
            } else {
                get_header($name);
            }
        }

        public function get_footer($name = null, $callback = null)
        {
            if ($this->ajax) {
                if (!$this->is_ajax()) {
                    get_footer($name);
                }
                if (isset($callback)) {
                    call_user_func($callback, $name);
                }
            } else {
                get_footer($name);
            }
        }

        public function get_sidebar($name = null)
        {
            $sidebar_name = (is_null($name) || empty($name)) ? "sidebar" : "sidebar-$name";

            if (file_exists(get_stylesheet_directory() . '/' . "$sidebar_name.php")) {
                get_sidebar($name);
            } else {
                if (!dynamic_sidebar($sidebar_name)) {
                    get_sidebar($name);
                }
            }
        }

        public function get_search_form($args = true)
        {
            get_search_form($args);
        }

        public function get_search_results_count()
        {
            global $wp_query;
            return $wp_query->found_posts;
        }

        /**
         * Filter for widget with no title
         * @param type $widget_title
         * @return string 
         */
        function widget_no_title($widget_title)
        {
            if (substr($widget_title, 0, 1) == '[' && substr($widget_title, -1) == ']') {
                return;
            }

            return $widget_title;
        }

        public function nav_menu($theme_location, $args = array())
        {
            $args = array_merge(array(
                'theme_location' => $theme_location,
                'container' => 'nav',
                'responsive' => false,
                'container_class' => 'menu-container',
                'container_id' => 'menu-' . $theme_location . '-container',
                'menu_id' => 'menu-' . $theme_location
                    ), $args);

            if ($args['responsive']) {
                $args['container_class'] .= ' menu-responsive';
                $this->jquery['menu-responsive'] = 'jQuery(".menu-responsive").responsiveMenu();';
                wp_enqueue_script('jquery-responsive-menu', get_template_directory_uri() . '/js/jquery.responsiveMenu.js', 'jquery', '0.1', true);
            }

            wp_nav_menu($args);
        }

        public function jquery_start()
        {
            ob_start();
        }

        public function jquery_end()
        {
            $output = ob_get_contents();
            ob_end_clean();
            $this->jquery[] = $output;
        }

        public function jquery()
        {
            if (!empty($this->jquery)) {
                echo 'jQuery(document).ready(function(){' . PHP_EOL . implode(PHP_EOL, $this->jquery) . PHP_EOL . '});' . PHP_EOL;
            }
        }

        public function pagination()
        {
            global $wp_query;

            $big = 999999999; // need an unlikely integer

            echo '<div class="navigation">';

            echo paginate_links(array(
                'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                'format' => '?paged=%#%',
                'current' => max(1, get_query_var('paged')),
                'total' => $wp_query->max_num_pages
            ));

            echo '</div>';
        }

        public function tiny_mce_before_init($settings)
        {

            $settings['style_formats'] = json_encode($this->settings['editor_classes']);

            return $settings;
        }

        /**
         * Add the Styles dropdown to the visual editor
         *
         * @param array $buttons Array of buttons already registered
         * @return array
         */
        public function mce_buttons_2($buttons)
        {
            array_unshift($buttons, 'styleselect');
            return $buttons;
        }

        public function image_size_names_choose($sizes)
        {
            $image_sizes = $this->get_elements('image_size_choose');

            foreach ($image_sizes as $image_size) {
                $sizes[$image_size[0]] = $image_size[1];
            }

            return $sizes;
        }

        function list_comments($args = array())
        {
            $args['callback'] = array($this, 'list_comments_callback');
            wp_list_comments($args);
        }

        function list_comments_callback($comment, $args, $depth)
        {
            $GLOBALS['comment'] = $comment;
            switch ($comment->comment_type) :
                case 'pingback' :
                case 'trackback' :
                    // Display trackbacks differently than normal comments.
                    ?>
                    <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
                        <p><?php _e('Pingback:', $this->textdomain); ?> <?php comment_author_link(); ?> <?php edit_comment_link(__('(Edit)', $this->textdomain), '<span class="edit-link">', '</span>'); ?></p>
                        <?php
                        break;
                    default :
                        // Proceed with normal comments.
                        global $post;
                        ?>
                    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
                        <article id="comment-<?php comment_ID(); ?>" class="comment">
                            <header class="comment-meta comment-author vcard">
                        <?php
                        echo get_avatar($comment, 44);
                        printf('<cite class="fn">%1$s %2$s</cite>', get_comment_author_link(),
                                // If current post author is also comment author, make it known visually.
                                ( $comment->user_id === $post->post_author ) ? '<span> ' . __('Post author', $this->textdomain) . '</span>' : ''
                        );
                        printf('<a href="%1$s"><time pubdate datetime="%2$s">%3$s</time></a>', esc_url(get_comment_link($comment->comment_ID)), get_comment_time('c'),
                                /* translators: 1: date, 2: time */ sprintf(__('%1$s at %2$s', $this->textdomain), get_comment_date(), get_comment_time())
                        );
                        ?>
                            </header><!-- .comment-meta -->

                        <?php if ('0' == $comment->comment_approved) : ?>
                                <p class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.', $this->textdomain); ?></p>
                        <?php endif; ?>

                            <section class="comment-content comment">
                        <?php comment_text(); ?>
                        <?php edit_comment_link(__('Edit', $this->textdomain), '<p class="edit-link">', '</p>'); ?>
                            </section><!-- .comment-content -->

                            <div class="reply">
                        <?php comment_reply_link(array_merge($args, array('reply_text' => __('Reply <span>&darr;</span>', $this->textdomain), 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
                            </div><!-- .reply -->
                        </article><!-- #comment-## -->
                        <?php
                        break;
                endswitch; // end comment_type check
            }

        }

        if (!function_exists('get_the_post_thumbnail_src')) {

            function get_the_post_thumbnail_src($post_id = null, $size = 'post_thumbnail')
            {
                $post_id = isset($post_id) ? $post_id : get_the_ID();
                return (preg_match('~\bsrc="([^"]++)"~', get_the_post_thumbnail($post_id, $size), $matches)) ? $matches[1] : '';
            }

        }

        if (!function_exists('get_post_parent')) {

            function get_post_parent($post = null)
            {

                global $wp_the_query;

                if (!$post)
                    $post = $wp_the_query->post;

                if (!is_object($post) || is_wp_error($post))
                    return false;

                if (!$post->post_parent)
                    return false;

                $parent = get_post($post->post_parent);

                return $parent;
            }

        }

        if (!function_exists('get_post_parent_id')) {

            function get_post_parent_id($id = null)
            {
                if (!isset($id)) {
                    global $post;
                    return $post->post_parent;
                } else {
                    if ($p = get_post($id)) {
                        return $p->post_parent;
                    }
                }

                return 0;
            }

        }

        if (!function_exists('get_post_superparent_id')) {

            function get_post_superparent_id($id = null, $level = 0, $post_type = 'page')
            {
                if (!isset($id)) {
                    global $post;
                    $id = $post->ID;
                }
                $ancestors = (array) get_ancestors($id, $post_type);

                if ($level > count($ancestors)) {
                    return false;
                }

                for ($i = 1; $i < $level; $i++) {
                    unset($ancestors[0]);
                    $ancestors = array_values($ancestors);
                }

                if ($level == 0)
                    $ancestors = array_reverse($ancestors);

                return empty($ancestors) ? $id : current($ancestors);
            }

        }

        if (!function_exists('get_post_level')) {

            function get_post_level($id = null)
            {
                if (!isset($id)) {
                    global $post;
                    $id = $post->ID;
                }
                $ancestors = (array) get_ancestors($id, 'page'); /* FIX! */
                return count($ancestors) + 1;
            }

        }

        if (!function_exists('get_taxonomy_ancestors')) {

            function get_taxonomy_ancestors($taxonomy_id, $taxonomy = 'category')
            {
                $ancestors = array();

                while (true) {
                    $parent_term = (int) get_term_by('id', $taxonomy_id, $taxonomy)->parent;
                    if ($parent_term == 0) {
                        break;
                    } else {
                        $ancestors[] = $parent_term;
                        $taxonomy_id = $parent_term;
                    }
                }

                return $ancestors;
            }

        }
        if (!function_exists('is_login')) {

            function is_login()
            {
                return in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'));
            }

        }
    }