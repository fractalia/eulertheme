<?php global $theme; ?>
<?php wp_footer(); ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.css3selectors.js">
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.placeholder.js">
    jQuery(document).ready(function(){
        jQuery.css3selectors();
        jQuery('[placeholder]').placeholder();
    });
</script>
<![endif]-->
<script>
    document.createElement('header');
    document.createElement('nav');
    document.createElement('section');
    document.createElement('article');
    document.createElement('aside');
    document.createElement('footer');
    document.createElement('time');
    <?php $theme->jquery(); ?>
</script>