<?php
global $theme;

$defaults = array(
    'search' => '',
    'placeholder' => __('Search', $theme->get_textdomain()),
    'submit_label' => __('Search', $theme->get_textdomain())
);

$args = is_array($echo) ? array_merge($defaults, $echo) : $defaults;
?>
<form method="get" id="searchform" action="<?php echo home_url('/'); ?>">
    <input autocomplete="off" type="search" value="<?php echo get_search_query(); ?>" name="s" id="s" <?php if ($args['placeholder'] !== false): ?>placeholder="<?php echo $args['placeholder']; ?>"<?php endif; ?> />
    <button id="searchsubmit"><?php echo $args['submit_label']; ?></button>
</form>
<?php $theme->jquery_start(); ?>
jQuery("form#searchform").on("submit", function(){
    if(jQuery.trim(jQuery(this).find("[name='s']").val()) == ""){
        jQuery(this).find("[name='s']").focus();
        return false;
    }
});
<?php $theme->jquery_end(); ?>
