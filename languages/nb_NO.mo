��    !      $      ,      ,     -     ?     L     b     i  p   z     �     �          #     (     0     5     K     ^  E   l     �     �     �     �  #   �          %  &   ,     S     Y     g  C   {  I   �  U   	  $   _  
   �  �  �          )     7  	   P     Z  e   m     �     �     �     	     	     !	     &	     ?	     X	  A   i	     �	     �	  	   �	     �	     �	     
     
  +   
     K
     S
     d
  O   |
  M   �
  W     '   r     �   % Comments &#187; %1$s at %2$s &larr; Older Comments (Edit) 1 Comment &#187; Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post. Comment navigation Comments Closed Comments are closed. Edit F jS, Y Logo Newer Comments &rarr; No Comments &#187; Nothing Found One thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; Pages: Permanent Link to %s Post author Posted in %s Read the rest of this entry &raquo; Reply <span>&darr;</span> Search Sorry, no posts matched your criteria. Tags: Theme Options Theme custom fields This entry was posted %1$s on %2$s at %3$s and is filed under %4$s. This post is password protected. Enter the password to view any comments. You can follow any responses to this entry through the <a href='%s'>RSS 2.0</a> feed. Your comment is awaiting moderation. l, F jS, Y Project-Id-Version: Euler Framework v0.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 
PO-Revision-Date: 2013-01-08 13:48:14+0000
Last-Translator: jcchavezs <jcchavezs@fractalia.pe>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Language: Spanish
X-Poedit-Country: SPAIN
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;
X-Poedit-Basepath: ../
X-Poedit-Bookmarks: 
X-Poedit-SearchPath-0: .
X-Textdomain-Support: yes %s kommentarer &#187; %1$s på %2$s &larr; Eldre kommentarer (Rediger) 1 kommentar &#187; Beklager, vi fant ingenting i dette arkivet. Kanskje et søk vil hjelpe deg finne det du leter etter? Kommentarnavigasjon Kommentering stengt Kommentering er stengt. Rediger d.m.y Logo Nyere kommentarer &rarr; Ingen kommentarer &#187; Ingenting funnet En tanke om  &ldquo;%2$s&rdquo; %1$s tanker om &ldquo;%2$s&rdquo; Sider: Permanentlenke til %s Forfatter Publisert i %s Les resten av innlegget &raquo; Svar <span>&darr;</span> Søk Beklager, vi kunne ikke finne noen innlegg. Tagger: Temaalternativer Theme tilpassede felter Dette innlegget ble publisert i %1$s de %2$s klokken %3$s og lagret under %4$s. Dette innlegget er passordbeskyttet. Skriv inn passord for å se kommentarer. Du kan følge svar til dette innlegget ved hjelp av <a href='%s'>RSS 2.0</a> strømmen. Kommentaren din venter på godkjenning. d.m.y 