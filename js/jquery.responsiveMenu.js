(function($) {
    $.fn.responsiveMenu = function(opts, value){
        this.each(function(){
            switch(typeof opts){
                case 'string':
                    var data = $(e).data('responsiveMenu-opts');
                    data[opts] = value;
                    $(e).data('responsiveMenu-opts', data);
                    break;
                default:
                    
                    opts = jQuery.extend({}, $.fn.responsiveMenu.options, opts);
            
                    var ul, select, text;
        
                    ul = $(this).data('responsiveMenu-opts', opts).children('ul.menu').addClass('menu-responsive-ul');
           
                    select = $('<select id="' + ul.attr('id') + '-select" class="menu-responsive-select" />').hide();
           
                    if(opts.selectNone !== false){
                        select.append('<option>' + opts.selectNone + '</option>');
                    }
           
                    ul.find('a').each(function(i, e){
                        text = $(e).text();
               
                        for(i = 1; i < $(e).parents('li').length; i++){
                            text = opts.pad + text;
                        }
                        select.append('<option value="' + $(e).attr('href') + '"' + ($(e).parent().hasClass(opts.currentClass) ? ' selected' : '') + '>' + text + '</option>');
                    });
           
                    select.appendTo($(this));
           
                    select.change(function() {
                        window.location = this.options[this.selectedIndex].value;
                    });

                    break;
            }

        });
    };
    
    $.fn.responsiveMenu.options = {
        currentClass: 'current-menu-item',
        pad: '--',
        selectNone: false
    };        
})(jQuery);