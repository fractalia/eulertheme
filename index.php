<?php global $theme; $theme->get_header(); ?>

<div id="content" class="narrowcolumn" role="main">

    <?php if (have_posts()) : ?>

        <?php while (have_posts()) : the_post(); ?>

            <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
                <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'euler'), the_title_attribute('echo=0')); ?>"><?php the_title(); ?></a></h2>
                <time><?php the_time(__('F jS, Y', 'euler')) ?></time>

                <div class="entry"><?php the_content(__('Read the rest of this entry &raquo;', 'euler')); ?></div>

                <p class="postmetadata"><?php the_tags(__('Tags:', 'euler') . ' ', ', ', '<br />'); ?> <?php printf(__('Posted in %s', 'euler'), get_the_category_list(', ')); ?> | <?php edit_post_link(__('Edit', 'euler'), '', ' | '); ?>  <?php comments_popup_link(__('No Comments &#187;', 'euler'), __('1 Comment &#187;', 'euler'), __('% Comments &#187;', 'euler'), '', __('Comments Closed', 'euler')); ?></p>
            </article>

        <?php endwhile; ?>

        <?php echo $theme->paginate(); ?>

    <?php else : ?>

        <article id="post-0" class="post no-results not-found">
            <header class="entry-header">
                <h1 class="entry-title"><?php _e('Nothing Found', 'euler'); ?></h1>
            </header><!-- .entry-header -->

            <div class="entry-content">
                <p><?php _e('Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'euler'); ?></p>
                <?php get_search_form(); ?>
            </div><!-- .entry-content -->
        </article><!-- #post-0 -->

    <?php endif; ?>

</div>

<?php $theme->get_footer(); ?>
